/*Name:Osayd Abdu; Course: CS600.120; Date: 6/17/2013;
 Homework ExtraCredit; (217)693-2577; Blackboard login:
 Oabdu1; Email: osaid3@live.com
*/
#ifndef LIST_H
#define LIST_H

typedef struct {
  char  name[21];
  int health;
}  Player ;

struct node {
  Player player;
  // char *name;
  struct node * next;
} ;

typedef struct node Node;
typedef Node * List;
typedef Node * NodePtr;
void printList(List);
void addTail(List *, Player);
int delete(List *, Node);   
void clearList(List *);  
NodePtr find(List, const char*);
NodePtr insert(List *, Player);
#endif
