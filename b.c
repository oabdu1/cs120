#include <stdio.h>
#include <ctype.h>
/*name:Osayd Abdu; CS600.120; 6/4/2013; Homework 1a; (217)693-2577; Blackboard login: Oabdu1; email: osaid3@live.com*/

int mask1 = 0xff;
int mask2 = 0xff00;
int mask3= 0xff0000;
int DToX(int num);
int BlackOrWhite(int R, int G, int B);
void FirstByte(int num1, int RGB1[]);
void SecondByte(int num2, int RGB2[]);
void ThirdByte(int num3, int RGB3[]);

int main(void){
  printf("<html><table>");
  int flag=0,flag2=0, num[]={0,0,0};
  char c=0;
  int checkpoint=0, i=0;
  int RGB1[3], RGB2[3], RGB3[3];
  printf("Enter three integers between 0 and 2^32 - 1 (4294967295)\n"); 
  while (c!=EOF){ 
    printf("<tr>");
    for (; (((c=getchar())>='0') && (c <='9'))||((c==' ' )||(c=='\n'));){
      if (isspace(c)||(c=='\n')){
	if (!flag2)
	  continue;
	else if (i==2){
	  flag=1;
	  flag2=0;
	  i=0;
	  break;}
	++checkpoint;
	continue;
      }
      else if (checkpoint){
	++i;
	checkpoint=0;
      }
      num[i]=(c-'0')+num[i]*10;
      flag2=1;
    }
    if(!flag){
    printf("Invalid character\n");
    continue;}
    else if(c==EOF)
      continue;
    FirstByte (num[0], RGB1);
    SecondByte (num[1], RGB2);
    ThirdByte (num[2], RGB3);
    int R1= (DToX(RGB1[0])/16), R2= (DToX(RGB1[0]) - R1*16);
    int G1= (DToX(RGB1[1])/16), G2= (DToX(RGB1[1]) - G1*16);
    int B1= (DToX(RGB1[2])/16), B2= (DToX(RGB1[2]) - B1*16);
    int TxtColor=BlackOrWhite(DToX(RGB1[0]),DToX(RGB1[1]), DToX(RGB1[2]));
    printf("<td bgcolor=\"#%x%x%x%x%x%x\"><font color=\"#%x%x%x%x%x%x\">#%x%x%x%x%x%x</font></td>", R1,R2,G1,G2,B1,B2,TxtColor,TxtColor,TxtColor,TxtColor,TxtColor,TxtColor,R1,R2,G1,G2,B1,B2);
    R1= (DToX(RGB2[0])/16), R2= (DToX(RGB2[0]) - R1*16);
    G1= (DToX(RGB2[1])/16), G2= (DToX(RGB2[1]) - G1*16);
    B1= (DToX(RGB2[2])/16), B2= (DToX(RGB2[2]) - B1*16);
    TxtColor=BlackOrWhite(DToX(RGB1[0]),DToX(RGB1[1]), DToX(RGB1[2]));
    printf("<td bgcolor=\"#%x%x%x%x%x%x\"><font color=\"#%x%x%x%x%x%x\">#%x%x%x%x%x%x</font></td>", R1,R2,G1,G2,B1,B2,TxtColor,TxtColor,TxtColor,TxtColor,TxtColor,TxtColor,R1,R2,G1,G2,B1,B2);
    R1= (DToX(RGB3[0])/16), R2= (DToX(RGB3[0]) - R1*16);
    G1= (DToX(RGB3[1])/16), G2= (DToX(RGB3[1]) - G1*16);
    B1= (DToX(RGB3[2])/16), B2= (DToX(RGB3[2]) - B1*16);
    TxtColor=BlackOrWhite(DToX(RGB1[0]),DToX(RGB1[1]), DToX(RGB1[2]));
    printf("<td bgcolor=\"#%x%x%x%x%x%x\"><font color=\"#%x%x%x%x%x%x\">#%x%x%x%x%x%x</font></td>", R1,R2,G1,G2,B1,B2,TxtColor,TxtColor,TxtColor,TxtColor,TxtColor,TxtColor,R1,R2,G1,G2,B1,B2);
    for (int p=0; p<3; p++){
      num[p]=0;}
    R1= (DToX(RGB1[0])/16), R2= (DToX(RGB1[0]) - R1*16);
    G1= (DToX(RGB2[1])/16), G2= (DToX(RGB2[1]) - G1*16);
    B1= (DToX(RGB3[2])/16), B2= (DToX(RGB3[2]) - B1*16);
    TxtColor=BlackOrWhite(DToX(RGB1[0]),DToX(RGB1[1]), DToX(RGB1[2]));
    printf("<td bgcolor=\"#%x%x%x%x%x%x\"><font color=\"#%x%x%x%x%x%x\">#%x%x%x%x%x%x</font></td>", R1,R2,G1,G2,B1,B2,TxtColor,TxtColor,TxtColor,TxtColor,TxtColor,TxtColor,R1,R2,G1,G2,B1,B2);
    printf("</tr>");
	   }
  printf("</table></html>");
  return 0;
	   }

void FirstByte(int num1, int RGB1[]){
  RGB1[2]= mask1 & num1;
  RGB1[1]= (mask2 & num1)>>8;
  RGB1[0]= (mask3 & num1)>>16;
} 
void SecondByte(int num2, int RGB2[]){
  RGB2[2]= mask1 & num2;
  RGB2[1]= (mask2 & num2)>>8;
  RGB2[0]= (mask3 & num2)>>16;
}
void ThirdByte(int num3, int RGB3[]){
  RGB3[2]= mask1 & num3;
  RGB3[1]= (mask2 & num3)>>8;
  RGB3[0]= (mask3 & num3)>>16;
}
int DToX(int num){
  char digits[10];
  int reminder=0, result=0X0;
  for (int j=0;j<10;j++){
    digits[j]=0;}
  int i=0;
  for (;(num >16)&&(i<10);i++){
    reminder=num;
    num/=16;
    reminder%=16;
    digits[i]= reminder;}
  digits[i]=num;
  for(int k=i;k>=0 ; k--){
  switch (digits[k]){
  case 10:
    result=0XA+result*16;
    break;
  case 11:
    result=0XB+result*16;
    break;
  case 12:
    result=0XC+result*16;
    break;
  case 13:
    result=0XD+result*16;
    break;
  case 14:
    result = 0XE + result*16;
    break;
  case 15:
    result=0XF+result*16;
    break;
  default:
    result=digits[k]+result*16;
    break;}}
  return result;
}
int BlackOrWhite(int R, int G, int B){
  int Hex=0X0;
  if (R==0)
    Hex=(G+B)/2 ;
  else 
    Hex=(R+G+B)/3;
  if (Hex>=0X88)
    return 0x0;
  else
    return 0xf;
}
