/*Name:Osayd Abdu; Course: CS600.120; Date: 6/17/2013;
 Homework ExtraCredit; (217)693-2577; Blackboard login:
 Oabdu1; Email: osaid3@live.com
Reflection:As this homework took about 7 hours, I 
consider it one of the easiest assignments and the
most interesting one. I like woking with likned lists. 
I felt while I was developing this program that I am
a real develper because I had a program and I only
needed to make it work with a better code. So, it
similar to a real job assignment.
*/
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "list.h"
#define FIREBALL_DAMAGE 30
#define ARROW_DAMAGE 40

//Global variables
Player *players;
int NUM_PLAYERS;		     
List mylist = NULL;
int getPlayerIndex(const char *name);
bool gameOver();
bool isAlive(Node playerNode);
void printStatus(const List lptr);
void fireball(const Node source, Node *target);
void arrow(const Node source, Node *target);



int main(int argc, char *argv[])
{
    char name[21];   
    Player defaultPlayer;
    if (argc != 2) {
	printf("Usage: game numPlayers\n");
	return 0;
    }
    NUM_PLAYERS = atoi(argv[1]);
    printf("enter the players names\n");
    for (int i = 0; i < NUM_PLAYERS; i++) {
      scanf("%s", name);
      strcpy(defaultPlayer.name,name);
      defaultPlayer.health=100;
      addTail(&mylist, defaultPlayer);
      
    }
    NodePtr targetNode;
    List head=mylist;
    List tempNode=mylist;
    while (!gameOver()) {
	printStatus(mylist);
	printf("\n%s's turn\n", (tempNode->player).name); 
	char attack[10], target[10];
	scanf("%s %s", attack, target);
       	targetNode=find(head, target);
	if (strcmp(attack, "fireball") == 0)
	    fireball(*head, targetNode);
	else if (strcmp(attack, "arrow") == 0)
	    arrow(*head, targetNode);
	else
	  printf("Invalid spell!\n");
	if (tempNode->next!=NULL)
	  tempNode=tempNode->next;
	else 
	  tempNode=mylist;
    }

    printf("winner is %s!\n", (mylist->player).name);	// shorthand for players[0].name
    clearList(&mylist);
    return 0;
}
bool gameOver()
{
    int count = 0;
    List myTEMPlist=mylist;

    while (myTEMPlist!=NULL) 
      {
	if (isAlive(*myTEMPlist)){
	  count++;
	  	myTEMPlist=myTEMPlist->next;
	}
	else 
	  {
	    printf("%s is DEAD\n", (myTEMPlist->player).name);
	    Node tempNode=*myTEMPlist;
	    myTEMPlist=myTEMPlist->next;
	    delete(&mylist,tempNode);
	  }

      }

    return count == 1;
}

bool isAlive(Node playerNode)
{
  // checkValidPlayer(playerNode);
    return (playerNode.player).health > 0;
}

void printStatus(const List lptr)
{
  List templist=lptr;
  do{
    printf("%s now has %d health, ", (templist->player).name, (templist->player).health);
    templist=templist->next;
  }while (templist!=NULL);
  

}

void fireball(const Node source, Node *target)
{

    (target->player).health -= FIREBALL_DAMAGE;
    printf("%s cast fireball at %s, dealing %d damage\n",
	   (source.player).name, (target->player).name, FIREBALL_DAMAGE);
}

void arrow(const Node source, Node *target)
{
  (target->player).health -= ARROW_DAMAGE;

  printf("%s shot an arrow at %s, dealing %d damage\n",
	 (source.player).name, (target->player).name, ARROW_DAMAGE);
}
