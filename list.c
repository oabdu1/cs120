/*Name:Osayd Abdu; Course: CS600.120; Date: 6/17/2013;
 Homework ExtraCredit; (217)693-2577; Blackboard login:
 Oabdu1; Email: osaid3@live.com
*/
#include "list.h"
#include "Player.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void printList(List head) 
{
  while (head != NULL) 
  {
    printf("Name: %s, Health: %d\n", (head->player).name
	   ,(head->player).health);
    head = head->next;
  }
  printf("\n");
}
// add int to end of list
void addTail (List *lptr, Player val) 
{
  NodePtr np = malloc(sizeof(Node));
  np->player = val;
  np->next = NULL;
  if (*lptr == NULL)
	{
	  *lptr = np;
	}
  else 
	{
	  NodePtr head = *lptr;
	  while (head->next != NULL)
		head = head->next;
	  head->next = np;
	}
}
/* delete int from list if it is there, return 1
 success, 0 failure*/
int delete(List *lptr, Node val) 
{
  NodePtr curr = *lptr;
  NodePtr prev = NULL;
  while (curr && strcmp((curr->player).name,
			(val.player).name)!=0)
	{
	  prev = curr;
	  curr = curr->next;
	}
  if (! curr)  // didn't find value
	return 0;
  if (prev == NULL ) // deleting first node
	*lptr = curr->next;
  else
	prev->next = curr->next;
  curr->next = NULL;
  free(curr);
  return 1;
}
// get rid of entire list
void clearList(List *lptr)
{
  if (*lptr != NULL)
	clearList(&(*lptr)->next);
  free(*lptr);
  *lptr = NULL;
}

/* find a value in the list, return pointer
 to containing node, or NULL*/
NodePtr find(List head, const char *name) 
{
  while (head && strcmp((head->player).name, name)!=0)
	head = head->next;
  return head;
}
