#include <stdio.h>
#include <math.h>
#include <stdlib.h>


void subtract(float a[3], float b[3], float c[3]);
void cross(float a[3], float b[3], float c[3]);
void normalize(float a[3]);
void computeNormal(float v1[3], float v2[3], float v3[3], float normal[3]);
int main()
{
  float v[128][3];
  float vn[128][3];
  int f[128][3];
  FILE *fptr, *fptr2;
  fptr=fopen("box.obj", "r");
  if (fptr==NULL)
    printf("Error!!! The file cannot be opened!!\n please make sure you have a file named \"input.obj\" in this current dirctory so the program can run\n");
  fptr2=fopen("output.obj", "w");
  char c,s[3];
  int vCounter=0,fCounter=0, commentsCounter=0, flag=0;
	//Read the vertices from input.obj into v and write them to output.obj
  for (int i=0; c!=EOF; i++){
    c=fgetc(fptr);
    if (c=='v'){
      fputc(c, fptr2);
      for (int j=0; j<3; j++){
	fscanf(fptr, "%f", &v[vCounter][j]);
      }     
      fprintf(fptr2," %f %f %f\n",v[vCounter][0],v[vCounter][1],v[vCounter][2]);
      ++vCounter; 
    }
    else if (c=='f'){
      for (int q=0; q<3; q++)
	fscanf(fptr, "%d", &f[fCounter][q]);
      fCounter++;}
    else if (c=='#'){
      if (!flag){
	commentsCounter++;
	fputc(c, fptr2);}
      while(c!='\n'){
	c=fgetc(fptr);
	if(!flag)
	  fputc(c, fptr2);
      }}
    else if(c=='s'){
      fputc(c, fptr2);
      fscanf(fptr, "%s", s);
      fprintf(fptr2, " %s\n", s);
      flag=1;
    }  
  }
  fclose(fptr);
  fclose(fptr2);

  for(int w=0; w<fCounter; w++){
    computeNormal(v[f[w][0]-1],v[f[w][1]-1],v[f[w][2]-1],vn[w]);
  }
  fptr2=fopen("output.obj", "a");
  fptr=fopen("box.obj", "r");
  for(int g=0; g<fCounter; g++){
    fprintf(fptr2, "vn %f %f %f\n", vn[g][0],vn[g][1],vn[g][2]);
  }
  
  for (int i=0;(c=fgetc(fptr))!=EOF; ){
    if (c=='#' && commentsCounter>0)
	commentsCounter--;
    else if(c=='#'){
      while (c!='\n'){
	fputc(c,fptr2);
	c=fgetc(fptr);
      }
      fputc(c,fptr2);
    }
    else if (c == 'f'){
      fprintf(fptr2, "f %d//%d %d//%d %d//%d\n", f[i][0],i+1,f[i][1],i+1,f[i][2],i+1);
      i++;
    }
    else
      while(c!='\n')
	c=fgetc(fptr); 
  }
	return 0;
}


/** Subtracts a from b and stores the result in c.
*/
void subtract(float a[3], float b[3], float c[3])
{
  //Fill this in
  c[0]=b[0]-a[0];
  c[1]=b[1]-a[1];
  c[2]=b[2]-a[2];
}

/** Computes the cross product of a and b, storing the result in c.
 */
void cross(float a[3], float b[3], float c[3])
{
	c[0] = a[1]*b[2]-a[2]*b[1];
	c[1] = a[2]*b[0]-a[0]*b[2];
	c[2] = a[0]*b[1]-a[1]*b[0];
}

/** Sets the length of a vector to 1.0.
 */
void normalize(float a[3])
{
  float length = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
	a[0] /= length;
	a[1] /= length;
	a[2] /= length;
}

/** Computes the normal of a triangle specified by vertices v1, v2,
    and v3, storing the normalized result in normal.
*/
void computeNormal(float v1[3], float v2[3], float v3[3], float normal[3])
{
  float a[3], b[3];
  subtract(v1, v2, a);
  subtract(v1, v3, b);
  cross(a, b, normal);
  normalize(normal);
  }
