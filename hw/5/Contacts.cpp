/*Name:Osayd Abdu; Course: CS600.120; Date: 6/18/2013; Homework 5;
(217)693-2577; Blackboard login: Oabdu1;
 Email: osaid3@live.com
*/
#include <string>
#include <iostream>
#include <vector>
#include <cctype>
#include <algorithm>
#include "Contacts.h"
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
bool DeleteFromFavorite(vector<Contacts>& favorite){
  string name;

  while(true)
    {
      cout << "Enter the name of the person that you want to delete or 0 to show all the favorite contacts or -1 to go back"<<endl;
      cin>>name;
      if ( name == "0"){
	ShowContacts(favorite);
	continue;
      }
      if (name =="-1")
	break;
      else
	{ 
	  bool flag=false;
	  unsigned int i;
	  for (i=0; i<favorite.size(); i++)
	    if(((favorite[i].name).compare(name))==0)
	      {
		flag=true;
		break;
	      }
	  if (flag)
	    {
	      string option;
	      while(true)
		{
		  cout << "Are you sure you want to delete "<< name <<" from the favorite list:"<<endl;
		  cout << "1- yes."<< endl<<"2- No." <<endl;
		  cin >> option;
		  if (option =="1"||option =="yes")
		    {
		      cout <<name<< " has been deleted from the favorite list"<<endl;
		      favorite.erase(favorite.begin()+i);
		      break;
		    }
		  else if (option=="2"||option == "no")		  
		    break;
		  cout << "Either invalid name or you changed your mind."<<endl;
		}  
	      if (option =="1"||option =="yes")
		break;
	    }
	  else
	    cout << "Invalid name!!"<<endl;
	}

    }
  return false;
}

void AddToFavorite(vector<Contacts> &source, vector<Contacts> &target)
{ 
  cout << "Plese enter the contact name you want to add to the favorite list or 0 to show all contacts or -1 to go back";
  cout << endl;
  string tempName;
  Contacts tempContact;
  vector<Contacts>::iterator it; 
  while (cin>>tempName)
    {
      if (tempName =="0"){
	ShowContacts(source);
	cout << "Plese enter the contact name you want to add to the favorite list or 0 to show all contacts or -1 to go back"<<endl;
       continue;
      }
      if (tempName=="-1")
	break;

      bool flag=false;
      unsigned int i;
      for (i=0; i<source.size(); i++)
	if(((source[i].name).compare(tempName))==0){
	  flag=true;
	  break;
	}
      if (flag)
	{
	  tempContact.name=tempName; 
	  it=target.end();
	  target.insert(it, tempContact);
	  cout <<tempName<< " has been added to the favorite list"<<endl;
	  break;	  
	}
      cout << "Invalid name!"<<endl;
      // cout << "Plese enter the contact name you want to add to the favorite list or 0 to show all contacts or -1 to go back";
    }

  
}




string FavoriteMenu(){
  string favoriteOption="0";
  cout << "Please choose from the menue below:"<<endl;
  cout << "1- Show favorite."<<endl;
  cout << "2- Add a contact to the favorite lsit."<<endl; 
  cout << "3- Delete a contact from the favorit list."<<endl;
  cout << "4- Arrange favorite's contacts in alphabetical order."<<endl;
  cout << "5- Back to main menu."<<endl;
  cout << "6- Exit."<<endl;
  while(cin>>favoriteOption)
    {
      if (favoriteOption>="1" && favoriteOption<="6")
	return favoriteOption;
      else if(favoriteOption == "5")
	break;
      cout << "Invalid number!!!"<<endl;
      cout << "Please choose from the menue below:"<<endl;
      cout << "1- Show favorite."<<endl;
      cout << "2- Add a contact to the favorite lsit."<<endl; 
      cout << "3- Delete a contact from the favorit list."<<endl;
      cout << "4- Arrange favorite's contacts in alphabetical order."<<endl;
      cout << "5- Back to main menu."<<endl;
      cout << "6- Exit."<<endl;
    }
  return "0";
}








string MainMenu()
{
  string mainOption="0";
  cout << "Please choose from the menue below:"<<endl;
  cout << "1- Contacts."<<endl;
  cout << "2- Favorite list."<<endl; 
  cout << "3- Exit."<<endl;
  while(cin>>mainOption)
    {
      if (mainOption=="1"||mainOption=="2")
	return mainOption;
      else if(mainOption == "3")
	break;
      cout << "Invalid number!!!"<<endl;
      cout << "Please choose from the menue below:"<<endl;
      cout << "1- Contacts."<<endl;
      cout << "2- Favorite list."<<endl; 
      cout << "3- Exit."<<endl;
    }
return "0";
}

string ContactsMenu()
{
  string contacts="0";
  cout << "Please choose from the menue below:"<<endl;
  cout << "1- Show contacts."<<endl;
  cout << "2- Add a new contact."<<endl;
  cout << "3- Edit a contact." <<endl;
  cout << "4- Delet a contact." <<endl;
  cout << "5- Arrange contacts in alphabetical order." <<endl;
  cout << "6- Back to main menu."<<endl;
  cout << "7- Exit." <<endl;
  while(cin>>contacts)
    {
      if (contacts>="1"||contacts<="6")
	break;
      cout << "Invalid number!!!"<<endl;
      cout << "Please choose from the menue below:"<<endl;
      cout << "1- Show contacts."<<endl;
      cout << "2- Add a new contact."<<endl;
      cout << "3- Edit a contact." <<endl;
      cout << "4- Delet a contact." <<endl;
      cout << "5- Arrange contacts in alphabetical order." <<endl;
      cout << "6- Back to main menu."<<endl;
      cout << "7- Exit." <<endl;
    }
return contacts;
}

bool ShowContacts(vector<Contacts> &names)
{
  int vectorSize;
  bool flag=false;
  vectorSize=names.size();
  cout<<"\n\n";
  for (int i=0; i<vectorSize; i++)
    {flag=true;
      cout << i+1 <<"- Name: " <<names[i].name<<endl;
      if ((names[i].mobile)!=0)
	cout << "\tMobile number is: "<<names[i].mobile<<endl;
      if ((names[i].work)!=0)
	cout << "\tWork   number is: "<<names[i].work<<endl;
      if ((names[i].home)!=0)
	cout << "\tHome   number is: "<<names[i].home<<endl;
      if ((names[i].fax)!=0)
	cout << "\tFax    number is: "<<names[i].fax<<endl;
      cout<<"\n";
    }
  return flag;
}

void AddContact(vector<Contacts> &name){
  Contacts temp;
  vector<Contacts>::iterator it;
  if (name.size()!=0)
    it =name.end();
  else 
    it = name.begin();
  cout << "Please enter the name of the contact:"<<endl;
  cin >> temp.name;
  for (unsigned int i=0;i<(temp.name).size(); i++)
    temp.name[i]=tolower(temp.name[i]);
  temp.name[0]=toupper(temp.name[0]);
  cout << "Add Mobile number or 0 to skip:"<<endl;
  cin >> temp.mobile;
  cout << "Add Work number or 0 to skip:"<<endl;
  cin >> temp.work;
  cout << "Add Home number or 0 to skip:"<<endl;
  cin >> temp.home;
  cout << "Add Fax number or 0 to skip:"<<endl;
  cin >> temp.fax;
  name.insert(it, temp);
}

bool EditContact(vector<Contacts> &name, string person )
{
  bool flag=false;
  //vector<Contacts>::iterator i;
  unsigned int i;
  for (i=0; i<name.size(); i++)
    if(((name[i].name).compare(person))==0){
      flag=true;
      break;
    }
  if (!flag)
    return false;

  cout <<"Name: " <<(name[i]).name<<endl;
  if ((name[i]).mobile!=0)
    cout << "\tMobile number is: "<<(name[i]).mobile<<endl;
  if ((name[i]).work!=0)
    cout << "\tWork number is: "<<(name[i]).work<<endl;
  if ((name[i]).home!=0)
    cout << "\tHome number is: "<<(name[i]).home<<endl;
  if ((name[i]).fax!=0)
    cout << "\tFax number is: "<<(name[i]).fax<<endl;
  
  string contactName;
  long int temp;
  cout << "Please enter a new name of the contact or 0 to skip:"<<endl;
  cin >> contactName;
  if (contactName!="0")
    name[i].name=contactName;
  cout << "Enter a new Mobile number or 0 to skip or -1 to delete:"<<endl;
  cin >> temp;
  if (temp == -1)
    name[i].mobile=0;
  else if (temp != 0)
    name[i].mobile=temp;
  cout << "Enter a new Work number or 0 to skip or -1 to delete:"<<endl;
  cin >> temp;
  if (temp==-1)
    name[i].work=0;
  else if (temp != 0)
    name[i].work=temp;
  cout << "Enter a new Home number or 0 to skip or -1 to delete:"<<endl;
  cin >> temp;
  if (temp == -1)
    name[i].home=0;
  else if (temp != 0)
    name[i].home=temp;
  cout << "Enter a new Fax number or 0 to skip or -1 to delete:"<<endl;
  cin >> temp;
  if (temp == -1)
    name[i].fax=0;
  else if (temp != 0)
    name[i].fax=temp;
  return true;
}
bool DeleteContact(vector<Contacts> &names, string person)
{

 bool flag=false;
  //vector<Contacts>::iterator i;
 unsigned int target;
 for (target=0; target<names.size(); target++)
   if(((names[target].name).compare(person))==0){
     flag=true;
     break;
   }
 if (!flag)
   return false;
 cout << "Name: " <<names[target].name<<endl;
 cout << "\tMobile number is: "<<names[target].mobile<<endl;
 cout << "\tWork number is: "<<names[target].work<<endl;
 cout << "\tHome number is: "<<names[target].home<<endl;
 cout << "\tFax number is: "<<names[target].fax<<endl;
 cout << "Are you sure this is the contact information that you want to delete:"<<endl;
 
 string option;
 cout << "1- Yes."<< endl<<"2- No." <<endl;
 while(cin >>option){
    
   if (option =="1"||option =="2"||option =="yes"||option=="no")
     break;
   cout << "Invalid number."<<endl<<"Are you sure you want to delete this whole contact:"<<endl;
   cout << "1- yes."<< endl<<"2- No." <<endl;
 }  
 if (option=="1"||option == "yes")
   names.erase(names.begin()+target);
 else 
   return false;
 
return true;
}
  
void ArrangeContacts(vector<Contacts>& names){
  std::sort(names.begin(),names.end());
}
