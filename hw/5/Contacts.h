/*Name:Osayd Abdu; Course: CS600.120; Date: 6/18/2013; Homework 5;
(217)693-2577; Blackboard login: Oabdu1;
 Email: osaid3@live.com
*/

#include <string>
#include <vector>
#ifndef _CONTACTS_H
#define _CONTACTS_H

using std::string;
using std::vector;

struct Contacts{
  string name;
  unsigned long int mobile;
  unsigned long int work;
  unsigned long int home;
  unsigned long int fax;

  bool operator<(const Contacts& x) const
  {
    return name < x.name;
  }
  bool operator==(const Contacts& x) const
  {
    return name == x.name;
  }
};

string FavoriteMenu();
string MainMenu();
string ContactsMenu();
bool ShowContacts(vector<Contacts>&);
void AddContact(vector<Contacts>&);
void AddToFavorite (vector<Contacts>&, vector<Contacts>&);
bool EditContact(vector<Contacts>&, string);
bool DeleteContact(vector<Contacts>&, string);
bool DeleteFromFavorite(vector<Contacts>&);
void ArrangeContacts(vector<Contacts>&);



#endif
