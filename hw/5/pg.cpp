#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "Contacts.h"

using std::cout;
using std::cin;
using std::vector;
using std::endl;
using std::string;
int main (){
  string main="0";
  vector<Contacts> names;
  vector<Contacts> favorite;
  while ((main=MainMenu())!="0")
    {
      string ContactsOption, option;
      string name;
     
      while (main=="1")
	{
	  // static int contactNum=0;
	  ContactsOption=ContactsMenu();
	  if (ContactsOption=="1")
	    {
	      if (!ShowContacts(names))
		cout << "There is no contacts!!!!" << endl;
	    }
	  else if (ContactsOption=="2")
	    {
	      AddContact(names);
	    }
	  else if(ContactsOption== "3")
	    {	 
	      while (true)
		{
		  cout << "Enter the contact name that you want to edit or 0 to show all the contacts or -1 to go back"<<endl;
		  cin>>name;
		  if ( name == "0"){
		    if (!ShowContacts(names))
		      cout << "There is no contacts!!!!" << endl;
		    continue;
		  }
		  if (name=="-1") 
		    break;
		  if (!EditContact(names, name))
		    {
		      cout << "Invalid or unfound name" <<endl;
		      continue;
		    }
		  break;
		}
	    }
	  else if(ContactsOption== "4")
	    {  
	      while(true)
		{
		  cout << "Enter the name of the person that you want to delete or 0 to show all the contacts or -1 to go back"<<endl;
		  cin>>name;
		  if ( name == "0"){
		    ShowContacts(names);
		    continue;
		  }
		  if (name =="-1")
		    break;
		  if(!DeleteContact(names, name))
		    {
		      cout << "Either Invalid name or you changed your mind."<<endl;
		      continue;
		    }
		  cout<<name<<" has been deleted!"<<endl;
		  break;
		}
	    }
	  else if (ContactsOption =="5")
	    ArrangeContacts(names);
	  else if (ContactsOption== "6")
	    main = "0"; 
	  else if (ContactsOption =="7")
	    return 0;
	}	  

      
      while(main=="2")
	{
	  ContactsOption=FavoriteMenu();
	  if (ContactsOption == "1")
	    {
	      if (!ShowContacts(favorite))
		cout << "There is no contacts in the favorite list!!!!" << endl;
	    }
	  else if (ContactsOption =="2")
	    {
	      AddToFavorite(names, favorite);
	    }
	  else if (ContactsOption =="3")
	      DeleteFromFavorite(favorite);
	  else if (ContactsOption == "4")
	    ArrangeContacts(favorite);
	  else if (ContactsOption =="5")
	    break;
	  else if (ContactsOption =="6")
	    return 0;
	}
    }
}

  


