#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/*Name:Osayd Abdu; Course: CS600.120; Date: 6/7/2013; Homework 2; (217)693-2577; Blackboard login: Oabdu1;
 Email: osaid3@live.com*/

/*
Reflection: This program was easier than the previous one in the coding stage, but it was much more difficult to understand
what should be done. However, the most frustrating part was writing on a file while some parts must be without change
(the comments), other parts should be modified. Although it took me about 9 hours to finish it completely, I learned many 
useful skills, reading and writing files practically, especially using the functions fprintf(), fscanf(), fgetc(), and 
fputc(). I enjoyed spending that time on the assignment, particularly the time I spent on learning how to read and write 
files. Finally, I think the purpose of this assignment is to practice reading and writing files. So, If it were to me to 
redesign this assignment, I would have chosen an easier problem to understand that can still illustrate the reading and 
writing files. 
*/



//functions prototypes 
void subtract(float a[3], float b[3], float c[3]);
void cross(float a[3], float b[3], float c[3]);
void normalize(float a[3]);
void computeNormal(float v1[3], float v2[3], float v3[3], float normal[3]);

int main()
{
  float v[128][3];
  float vn[128][3];
  int f[128][3];
  FILE *fptr, *fptr2;
  fptr=fopen("input.obj", "r");
  if (fptr==NULL)
    printf("Error!!! The file cannot be opened!!\nplease make sure you have a file named \"input.obj\" in this current dirctory so the program can run\n");
  fptr2=fopen("output.obj", "w");
  // declaring the char "c" that will hold the read chars from the file.
  char c,s[3];
  int vCounter=0,fCounter=0, commentsCounter=0, flag=0;
  /*this loop will read the first char of each line and if it is # it will read the next chars untill it finds '\n'; if
 it is 'v', it will read the next three numbers and save them in the array v; if it is 'f', it will read the next three 
numbers and save them in the array f; if it is 's', it will read the next string -whether on or off- and save it in the 
array s; if the char is 'v', '#', or 's', it will immediately write it and what follows it to the output file because 
there will be no change in these particular data. Then the loop exits.
   */
  for (int i=0; c!=EOF; i++){
    c=fgetc(fptr);
    if (c=='v'){
      //writing this char into the output.obj
      fputc(c, fptr2);
      for (int j=0; j<3; j++){
	fscanf(fptr, "%f", &v[vCounter][j]);
      }     
      fprintf(fptr2," %f %f %f\n",v[vCounter][0],v[vCounter][1],v[vCounter][2]);
      ++vCounter; 
    }
    else if (c=='f'){
      for (int q=0; q<3; q++)
	fscanf(fptr, "%d", &f[fCounter][q]);
      fCounter++;}
    else if (c=='#'){
      if (!flag){
	commentsCounter++;
	fputc(c, fptr2);}
      while(c!='\n'){
	c=fgetc(fptr);
	/* the flag here is to make sure not to write any comments after the 's' because it will be written later.
	 */
	if(!flag)
	  fputc(c, fptr2);
      }}
    else if(c=='s'){
      fputc(c, fptr2);
      fscanf(fptr, "%s", s);
      fprintf(fptr2, " %s\n", s);
      flag=1;
    }  
  }
  //closing both files
  fclose(fptr);
  fclose(fptr2);
  // finding all the normals and store them in vn
  for(int w=0; w<fCounter; w++){
    computeNormal(v[f[w][0]-1],v[f[w][1]-1],v[f[w][2]-1],vn[w]);
  }
  //opening "output.obj" to append the normals and the the f array
  fptr2=fopen("output.obj", "a");
  /*opening "input.obj" and read over it again to make sure that no comments have been ignored when writing the output file*/
  fptr=fopen("input.obj", "r");
  //writing the normals to the output file
  for(int g=0; g<fCounter; g++){
    fprintf(fptr2, "vn %f %f %f\n", vn[g][0],vn[g][1],vn[g][2]);
  }
  /* going over "input.obj" again to write the faces their normals into "output.obj" and any other 
comments that might exist. 
   */
  for (int i=0;(c=fgetc(fptr))!=EOF; ){
    // to skip all the comments before the 's'
    if (c=='#' && commentsCounter>0)
	commentsCounter--;
    else if(c=='#'){
      while (c!='\n'){
	fputc(c,fptr2);
	c=fgetc(fptr);
      }
      fputc(c,fptr2);
    }
    else if (c == 'f'){
      fprintf(fptr2, "f %d//%d %d//%d %d//%d\n", f[i][0],i+1,f[i][1],i+1,f[i][2],i+1);
      i++;
    }
    else
      while(c!='\n')
	c=fgetc(fptr); 
  }
  //closing both files
  fclose(fptr);
  fclose(fptr2);
	return 0;
}


/** Subtracts a from b and stores the result in c.
*/
void subtract(float a[3], float b[3], float c[3])
{
  //Fill this in
  c[0]=b[0]-a[0];
  c[1]=b[1]-a[1];
  c[2]=b[2]-a[2];
}

/** Computes the cross product of a and b, storing the result in c.
 */
void cross(float a[3], float b[3], float c[3])
{
	c[0] = a[1]*b[2]-a[2]*b[1];
	c[1] = a[2]*b[0]-a[0]*b[2];
	c[2] = a[0]*b[1]-a[1]*b[0];
}

/** Sets the length of a vector to 1.0.
 */
void normalize(float a[3])
{
  float length = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
	a[0] /= length;
	a[1] /= length;
	a[2] /= length;
}

/** Computes the normal of a triangle specified by vertices v1, v2,
    and v3, storing the normalized result in normal.
*/
void computeNormal(float v1[3], float v2[3], float v3[3], float normal[3])
{
  float a[3], b[3];
  subtract(v1, v2, a);
  subtract(v1, v3, b);
  cross(a, b, normal);
  normalize(normal);
  }
