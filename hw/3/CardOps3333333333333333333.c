#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "CardOps.h"

// static here means only this file can access these external variables
static const char *SUITS[4] = {"Diamonds", "Clubs", "Hearts", "Spades"};
static int ace_value = 14;

/** Set the ace_value to be high (14).
*/
void setAceHigh(){
  ace_value=14;
  // 2 pts
}
/** Set the ace_value to be low (1).
*/
void setAceLow(){
  ace_value=1;
  // 2 pts
}
/** Validate the suit of a playing card, returning its relative order
    (1-4) if it appears in SUITS above (case insensitive) and 0
    (invalid) otherwise.  (Parameter type changed 6/7, 6:30 pm.)
*/



int suitValue(const char* suitPtr){
  char *charPtr=NULL;
  int suit_value=0;
  charPtr=(char*)malloc(sizeof(char) * (strlen(suitPtr)+1));
  strcpy(charPtr,suitPtr);
  for (unsigned int i=1; i<strlen(suitPtr);i++)
    *(charPtr+i)=tolower(*(charPtr+i));
  *charPtr=toupper(*(charPtr));
  for(int j=0;j<4; j++){
    if((strcmp(charPtr, *(SUITS+j))) == 0){
      suit_value= j+1;
      break;}
  }
  free(charPtr);
  return suit_value;
}
/** Calculate the face value of a playing card, returning 0 if it is
    invalid.  Remember that Jack = 11, Queen = 12, King = 13.  Use the
    current ace_value for any Aces.  Do case insensitive comparisons.
*/
int faceValue(const char* faceStr){
  if (isdigit(*faceStr)){
    int y=atoi(faceStr);
    if (y>1 && y<11)
      return y;}
  else if (isalpha(*faceStr)) {
    char *charPtr=NULL;
    charPtr=(char*)malloc(sizeof(char) * (strlen(faceStr)+1));
    strcpy(charPtr,faceStr);
    for (unsigned int i=0; i<strlen(faceStr);i++)
      *(charPtr+i)=tolower(*(charPtr+i));
    if (strcmp(charPtr,"jack")==0){
      free(charPtr);
      return 11;
    }
    else if (strcmp(charPtr,"queen")==0){
      free(charPtr);
      return 12;
    }
    else if (strcmp(charPtr,"king")==0){
     free(charPtr);
     return 13;
    }
    else if (strcmp(charPtr,"ace")==0){
      free(charPtr);      
      return ace_value;
    }
    free (charPtr);
      }
  
  return 0;
}
/** Rank is defined as 13 times the suit value, plus the face value.
*/
int rank(const char* suit, const int face){
  int suit_value=suitValue(suit);
  return suit_value*13+face;
}
/** Parse a card from a string in the form "10 of Clubs" or "King of
    Hearts".  The card's suit should be stored in parameter suit, and
    its face value in parameter face.  Return 0 if either the suit or
    face are invalid, and return the rank of the card otherwise.
	(2nd Parameter type and name changed 6/7, 6:30pm.)
*/
int parse(const char* cardString, char* suit, int* facePtr){
  char *chPtr=NULL;
  int fValue=0;
  int faceIndex=0,suitIndex=0;
  int length=strlen(cardString);

  for(;!isspace(*(cardString+faceIndex));)
    faceIndex++;
  chPtr= (char*)malloc(sizeof(char)*faceIndex+1);
  for(int i=0; i<faceIndex; i++)
    *(chPtr+i)=*(cardString+sizeof(char)*i);
  *(chPtr+faceIndex)='\0';
  fValue=faceValue(chPtr);
  suitIndex=length-1;
  for(;!isspace(*(cardString+suitIndex));)
    suitIndex--;
  chPtr= (char*)realloc(chPtr, sizeof(char)*(length-suitIndex+1));
  int counter=0;
  for(int j=suitIndex+1; j<length; j++){
    *(chPtr+counter)=*(cardString+sizeof(char)*j);
    counter++;
  }
  for (unsigned int i=0; i<5;i++)
    *(chPtr+i)=tolower(*(chPtr+i));
  if(strncmp(chPtr, "clubs", 5)==0)
    *(chPtr+counter-1)='\0';
  else
    *(chPtr+counter)='\0';




  if ((fValue==0)||(suitValue(chPtr)==0)){
    free(chPtr);
    return 0;
  }
  *facePtr=fValue;
  int m=0;
  for (; (*(chPtr+m))!='\0';m++)
       *(suit+m)=*(chPtr+m);
   *(suit+m)=*(chPtr+m);
  free(chPtr);
  return rank(suit, fValue);
}
