#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "CardOps.h"

/*Name:Osayd Abdu; Course: CS600.120; Date: 6/24/2013; Homework 3; (217)693-2577; Blackboard login: Oabdu1;
 Email: osaid3@live.com*/

typedef struct
{
  int face;
  char suit[9];
  int rank;
} CARD;
int UniqueValues (const CARD * ptr, const int i);
int
main (int argc, char *argv[])
{

  if (argc != 2)
    {
      printf ("Usage: pg3a <FileName>\n");
      return 0;
    }
  FILE *fptr;
  fptr = fopen (argv[1], "r");
  //char c = 0;
  char *cardLine = NULL, *checkPtr;
  CARD *cardStr = NULL;

  cardStr = (CARD *) malloc (sizeof (CARD) * 5);
  int i = 0, counter = 1, newSize = 0, UniqueTemp=0;
  while (counter)
    {
      if (i >= 5 * counter)
	{
	  counter++;
	  newSize = counter * 5;
	  cardStr = (CARD *) realloc (cardStr, sizeof (CARD) * newSize);

	}
      cardLine = (char *) malloc (sizeof (char) * 31);	//the maximuim chars at a line should be 30. 
      checkPtr=fgets(cardLine, 30,fptr);
      if (checkPtr==NULL){
	free(cardLine);
	break;
      }
      if (*(cardLine+(strlen(cardLine)-1))=='\n')
	*(cardLine+(strlen(cardLine)-1))='\0';
      (cardStr + i)->rank = parse (cardLine, (char *) &((cardStr + i)->suit),(int *) &((cardStr + i)->face));
      if (((cardStr + i)->rank) == 0)
	  i--;
      else if ((UniqueTemp=UniqueValues(cardStr, i))== 0)
	  --i;
      free (cardLine);
      ++i;
    }
  fclose (fptr);
  CARD temp;
  for (int l = 1; l < i; l++)
    {
      for (int k = l;k > 0 && ((cardStr + k)->rank) < ((cardStr + k - 1)->rank); k--)
	{
	  temp = *(cardStr + k);
	  *(cardStr + k) = *(cardStr + k - 1);
	  *(cardStr + k - 1) = temp;
	}
    }

  for (int q = 0; q < i; q++)
    {
      printf ("rank:%d, face:%d, suit:%s\n", (cardStr + q)->rank,
	      (cardStr + q)->face, (cardStr + q)->suit);
    }

  free (cardStr);

  return 0;
}


int
UniqueValues (const CARD * ptr, const int i)
{
  for (int j = 0; j < i; j++)
    {
      if ((ptr+i)->face == (ptr+j)->face)
	if ((strcmp ((ptr+i)->suit, (ptr+j)->suit)) == 0)
	  return 0;
    }
  return 1;
}
