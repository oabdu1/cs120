/* This is a header file for a collection of functions to work with
   playing cards from a standard deck..
*/
/*Name:Osayd Abdu; Course: CS600.120; Date: 6/7/2013; Homework 2; (217)693-2577; Blackboard login: Oabdu1;
 Email: osaid3@live.com*/
#ifndef CARDOPS_H
#define CARDOPS_H

/** Set the ace_value to be high (14).
*/
void setAceHigh();
// 2 pts

/** Set the ace_value to be low (1).
*/
void setAceLow();
// 2 pts

/** Validate the suit of a playing card, returning its relative order
    (1-4) if it appears in SUITS above (case insensitive) and 0
    (invalid) otherwise.  (Parameter type changed 6/7, 6:30 pm.)
*/
int suitValue(const char* suitPtr);
// 6 pts

/** Calculate the face value of a playing card, returning 0 if it is
    invalid.  Remember that Jack = 11, Queen = 12, King = 13.  Use the
    current ace_value for any Aces.  Do case insensitive comparisons.
*/
int faceValue(const char* faceStr);
// 7 pts

/** Rank is defined as 13 times the suit value, plus the face value.
*/
int rank(const char* suit, const int face);
// 3 pts

/** Parse a card from a string in the form "10 of Clubs" or "King of
    Hearts".  The card's suit should be stored in parameter suit, and
    its face value in parameter face.  Return 0 if either the suit or
    face are invalid, and return the rank of the card otherwise.
	(2nd Parameter type and name changed 6/7, 6:30pm.)
*/
int parse(const char* cardString, char* suit, int* facePtr);
// 5 pts

#endif
