/*Name:Osayd Abdu; Course: CS600.120; Date: 6/21/2013; Homework 6;
(217)693-2577; Blackboard login: Oabdu1; Email: osaid3@live.com


Reflection: This assignment is one of the easiest assignments in 
this semester because the instructions were very clear and it was 
very helpful to learn about classes and templates. Although we had 
several classes about using and dealing classes in C++, I had 
never been able to understand the concept behind them until I start 
working on this assignment. In addition, I was enjoying while I was
 doing this assignment and it only took me 8 hours to finish to completely.
*/





#ifndef _BOARD_CPP
#define _BOARD_CPP
#include <string>
#include "Color.h"
#include <sstream>
#include <stdexcept>
using std::string;
using std::endl;
template <class T>
class Board{
  struct Cell{
    Color background;
    Color foreground;
    T cell;
  };
  int rows, cols;
  Cell **boardcells;
public:
  Board(int rows)
  {
    this->rows=rows;
    cols=rows;
    boardcells=new Cell*[rows];
    for (int i=0;i<rows;i++)
      boardcells[i]=new Cell[cols];
  }
  Board(int rows, int cols) //default contructor
  {
    this->cols=cols; 
    this->rows=rows;
    boardcells=new Cell*[rows];
    for (int i=0; i<rows; i++)
      boardcells[i]=new Cell[cols];
  }
  Board(const Board &b)
  {
    rows=b.rows;
    cols=b.cols;
    boardcells=new Cell*[rows];
    for(int i=0; i<rows;i++)
      {
	boardcells[i]=new Cell[cols];
	for (int j=0; j<cols; j++)
	  boardcells[i][j]=b.boardcells[i][j];
      }
  }
  //deconstructor 
  ~Board()
  {
    for(int i=0; i<rows; i++)
      delete [] boardcells[i];
    delete [] boardcells;
  }
  string htmlTable()
  {
    std::stringstream ss;
    ss<<"<table>"<<endl;
    for(int i=0; i<rows;i++){
      ss<<"<tr>";
      for (int j=0;j<cols;j++){
	ss<<"<td bgcolor=\""<<((boardcells[i][j]).background.hex())<<"\"><font color=\""<<((boardcells[i][j]).foreground.hex())<<"\">"<<boardcells[i][j].cell<<"</font></td>";
      }
      ss<<"</tr>";
    }
    ss<<"</table>";
    string str=ss.str();
    return str;
  }
  bool setBackground(Color color, int row, int col)
  {
    if (row>rows||row<0||col>cols||col<0)
      return false;
    boardcells[row][col].background=color;
    return true;
  }
  bool setForeground(Color color, int row, int col)
  {
    if (row>rows||row<0||col>cols||col<0)
      return false;
    boardcells[row][col].foreground=color;
    return true;
  }
  bool setCell(T cell, int row, int col)
  {
    if (row>rows||row<0||col>cols||col<0)
      return false;
    boardcells[row][col].cell=cell;
    return true;

  }
  void initBoard(T cell)
  {
   for(int i=0; i<rows;i++)
      for (int j=0; j<cols; j++)
	boardcells[i][j].cell=cell;
  }
  T getCell(int row, int col)throw(std::range_error)
  {
    if (row>rows || col>cols ||row<0 ||col<0)
      throw std::range_error("Invalid indices!!!");
    return boardcells[row][col].cell;
  }
  T & getCellReference(int row, int col) throw(std::range_error)
  {
    if (row>rows || col>cols ||row<0 ||col<0)
      throw std::range_error("Invalid indices!!!");
    return (boardcells[row][col].cell);
  }
  Board getSub(int row1, int col1, int row2, int col2) throw(std::range_error)
  {
    if (row1>rows || col1>cols || row1<0 ||col1<0 || row2>rows || col2>cols || row2<0 || col2<0)
      throw std::range_error("Invalid indices!!!");
    int newRow=row2-row1+1;
    int newCol=col2-col1+1;
    Board newBoard(newRow, newCol);
    int newI=0, newJ=0;
    for (int i=row1; i<=row2; i++)
      {
	newJ=0;
	for(int j=col1; j<=col2; j++)
	  {
	    newBoard.setCell(boardcells[i][j].cell,newI,newJ);
	    newBoard.setBackground(boardcells[i][j].background,newI,newJ);
	    newBoard.setForeground(boardcells[i][j].foreground,newI,newJ);
	    newJ++;
	  }
	newI++;
      }
    return newBoard;
  }
};
#endif
