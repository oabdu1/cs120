// Test file for the Board template class
// Joanne Selinski, 6/18/2013

/************************

 WARNING: this test program has not been tested itself, not even compiled

 version 2 (6/19) - existing compilation errors have to do with the Board 
 class not having any functions defined yet.  If you define them properly,
 it should be ok.

************************/

#include "Board.cpp"
#include "Color.h"

#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>

using std::string;
using std::ofstream;
using std::ifstream;
using std::range_error;
using std::cerr;
using std::ios;
using std::endl;
using std::cout;

template<class T>
void colorBackCube(Board<T> & cube, Color clr) {
  for (int r=0; r < 3; r++)
	for (int c=0; c < 3; c++)
	  cube.setBackground(clr, r, c);
}


int main()
{
  Board<char> ttt(3);  // tic-tac-toe board
  Board<int> sdk(9);   // sudoku board
  Board<string> words(10,5);  // chart of words

  ofstream table("tables.html", ios::out);
  table << "<html>" << endl;

  Color grn(0,180,0);
  Color wht;
  Color red(120, 0, 0);
  Color blu("#66FFFF");
  bool set;
  int r, c;
  int num;

  ttt.initBoard(' ');
  for (num=0; num < 3; num++)
	ttt.setCell('O', num, num);
  table << "<p>first tic tac toe: </p>" << endl;
  table << ttt.htmlTable() << endl << endl;

  num = 1;
  for (r = 0; r < 9; r++) {
	num = r+1;
	for (c = 0; c < 9; c++) {
	  sdk.setBackground(grn, r, c);  // true
	  sdk.setForeground(wht, r, c);  // true
	  sdk.setCell(num, r, c);  // true
	  num = (num % 9) + 1;
	}
  }
  table << "<p>sudoku init: </p>" << endl;
  table << sdk.htmlTable() << endl << endl;

  Board<int> sub = sdk.getSub(6, 3, 8, 5);
  set = sub.setBackground(wht, 2, -20);  // false
  set = sub.setForeground(Color::black(), 200, 3);  // false
  set = sub.setForeground(wht, 2, -20);  // false
  set = sub.setBackground(Color::black(), 200, 3);  // false
  set = sub.setCell(24, 204, -20);  // false
  cout << set << endl;
  colorBackCube(sub, red);
  table << "<p>sudoku bottom middle sub: </p>" << endl;
  table << sub.htmlTable() << endl << endl;

  table << "<p>original sudoku: </p>" << endl;
  table << sdk.htmlTable() << endl << endl;

  Board<char> t2(ttt);  
  set = t2.setCell('X', 1, 1);  // true
  set = t2.setCell('O', 2, 3);  // false
  set = t2.setCell('*', -1, 2);  // false
  // bunch of good setCell calls
  t2.setCell('O', 0, 0);
  t2.setCell('O', 0, 1);
  t2.setCell('X', 0, 2);
  t2.setCell('X', 2, 0);
  table << "<p>2nd tic tac toe: </p>" << endl;
  table << t2.htmlTable() << endl << endl;

  // testing getCell calls
  char oh = t2.getCell(0, 0);  // 'O'
  char xx = t2.getCell(0, 2);  // 'X'
  table << "<p>oh and xx: " << oh << xx << "</p>" << endl;

  char &cr = t2.getCellReference(0, 0);
  cr = '$';
  t2.getCellReference(0, 2) = '@';
  oh = t2.getCell(0, 0);  // '$'
  xx = t2.getCell(0, 2);  // '@'
  table << "<p>after ref change oh and xx: " << oh << xx << "</p>" << endl;

  // invalid calls throw exception
  try {
	oh = t2.getCell(10, -1);
  }
  catch (const std::range_error& e) {
	cerr << "invalid: " << e.what() << endl;
  }
  try {
	oh = t2.getCell(-1, 10);
  }
  catch (const std::range_error& e) {
	cerr << "invalid: " << e.what() << endl;
  }
  try {
	cr = t2.getCellReference(-1, 0);
  }
  catch (const std::range_error& e) {
	cerr << "invalid: " << e.what() << endl;
  }
  try {
	cr = t2.getCellReference(0, -1);
  }
  catch (const std::range_error& e) {
	cerr << "invalid: " << e.what() << endl;
  }
  try {
	sub = sdk.getSub(-25, 0, 0, 25);
  }
  catch (const std::range_error& e) {
	cerr << "invalid: " << e.what() << endl;
  }

  colorBackCube(ttt, blu);
  table << "<p>first tic tac toe blue: </p>" << endl;
  table << ttt.htmlTable() << endl << endl;

  ifstream ifile("Color.h", ios::in);
  string word;
  for (r=0; r < 10; r++)
	for (c=0; c < 5; c++) {
	  ifile >> word; 
	  words.setCell(word, r, c);
	}
  string wtable = words.htmlTable();
  table << "<p>50 words from Color.h</p>" << endl;
  table << wtable << endl << endl;

  table << "</html>" << endl;
  table.close();
  ifile.close();

  return 0;
}

