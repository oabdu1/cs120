/*Name:Osayd Abdu; Course: CS600.120; Date: 6/21/2013; Homework 6;
(217)693-2577; Blackboard login: Oabdu1; Email: osaid3@live.com
*/

#ifndef _COLOR_H
#define _COLOR_H

#include <string>
class Color{
  int Red,Green, Blue;
  int Decimal;
  std::string Hexadecimal;
public:
  //cnostructors
  Color(); //default constructor
  Color(int Decimal);
  Color(int Red, int Green, int Blue);
  Color(std::string Hexadecimal); 
  // ~Color();
  //functions
  static Color black();
  int XtoI(std::string HEX);
  std::string ItoX (int D);
  std::string RGBtoHEX(const int r, const int g, const int b);
  void DtoRgb(int D);
  std::string rgb();
  int value();
  std::string hex();
  int changeRed(int red);
  int changeGreen(int green);
  int changeBlue(int blue);
};
#endif
