/*Name:Osayd Abdu; Course: CS600.120; Date: 6/21/2013; Homework 6;
(217)693-2577; Blackboard login: Oabdu1; Email: osaid3@live.com
*/
#include <iostream>
#include <string>
#include <sstream>
#include <cstdio>
#include "Color.h"

using std::string;
   //cnostructors
Color::Color(): Red(255), Green(255), Blue(255), Decimal(16777215), Hexadecimal("#ffffff")
{} //default constructor
Color::Color(int Decimal)
{
  if (Decimal>16777215)
    Decimal=16777215;
  if (Decimal<0)
    Decimal=0;
  this->Decimal=Decimal; 
  DtoRgb(Decimal);
  Hexadecimal= ItoX(Decimal);
}
Color::Color(int Red, int Green, int Blue)
{
  if (Red>255)
    Red=255;
  else if(Red<0)
    Red=0;
  this->Red=Red;
  if (Green>255)
    Green=255;
  else if(Green<0)
    Green=0;
  this->Green=Green;
  if (Blue>255)
    Blue=255;
  else if(Blue<0)
    Blue=0;
  this->Blue=Blue;    
  Hexadecimal=RGBtoHEX(Red, Green, Blue);
  Decimal=XtoI(Hexadecimal);
}
Color::Color(string Hexadecimal)
{
  this->Hexadecimal=Hexadecimal;
  Decimal=XtoI(Hexadecimal);
  DtoRgb(Decimal);
}   
Color Color::black()
{
  Color B(0);
  return B;
}
int Color::XtoI(string HEX)
{
  unsigned int i=0;
  HEX.erase(HEX.begin());
  sscanf(HEX.c_str(),"%X",&i);
  return i;
}
string Color::ItoX (int D)
{
  char Hex[8];
  Hex[0]='#';
  sprintf(Hex+1,"%.6x", D);
  return Hex; 
}
string Color::RGBtoHEX(const int r, const int g, const int b)
{
  string HexVal="#", strtemp;
  int temp;
  string a[]={"0","1","2","3","4","5","6","7","8","9"};
  for (int i=1;i<7;i++)
    {
      switch (i)
	{
	case 1:
	  temp =r/16;
	  break;
	case 2:
	  temp = r%16;
	  break;
	case 3:
	  temp =g/16;
	  break;
	case 4:
	  temp =g%16;
	  break;
	case 5:
	  temp =b/16;
	  break;
	case 6:
	  temp =b%16;
	  break;
	}
      switch(temp)
	{
	case 10:
	  HexVal+="a";
	  break;
	case 11:
	  HexVal+="b";
	  break;
	case 12:
	  HexVal+="c";
	  break;
	case 13:
	  HexVal+="d";
	  break;
	case 14:
	  HexVal+="e";
	  break;
	case 15:
	  HexVal+="f";
	  break;
	default:
	  strtemp=a[temp];
	  HexVal+= strtemp;
	  break;
	}
    }
  return HexVal;
}

void Color::DtoRgb(int D)
{
  int temp;
  temp = D/256;
  Blue =D%256;
  Green=temp%256;
  temp=temp/256;
  if (temp>255)
    Red=255;
  else 
    Red=temp;
}
string Color::rgb()
{
  char Hex[17];
  sprintf(Hex,"rgb(%d,%d,%d)", Red, Green, Blue);
  return Hex; 
}
int Color::value()
{
  return Decimal;
}
string Color::hex()
{
  return Hexadecimal;
}
int Color::changeRed(int red)
{
  int temp=Red;
  Red+=red;
  if (Red>255)
    Red=255;
  else if (Red<0)
    Red=0;
  return temp;
}
int Color::changeGreen(int green)
{
  int temp=Green;
  Green+=green;
  if (Green>255)
    Green=255;
  else if (Green<0)
    Green=0;
  return temp;
}
int Color::changeBlue(int blue)
{
  int temp=Blue;
  Blue+=blue; 
  if (Blue>255)
    Blue=255;
  else if (Blue<0)
    Blue=0;
  return temp;
}
