// test program for the Color class


#include <iostream>
#include <string>
#include "Color.h"
using std::cout;
using std::endl;
using std::string;

int main()
{
	Color c1;			           // should be white
	Color c2(123456);              // rgb(1, 226, 64)
	Color c3(13, 52, 250);
	Color crazy(-10, 400, 50);     // error correct to (0, 255, 50)
	Color c4("#33aa66");

	cout << "white is " << c1.value() << " " << c1.rgb() << " " << c1.hex() << endl;

	int val = c2.value();  
	string rgb = c2.rgb();
	string hex = c2.hex();
	cout << "c2 value is " << val << endl;   // 123456
	cout << "c2 rgb is " << rgb << endl;     // rgb(1,226,64)
	cout << "c2 hex is " << hex << endl;     // #01e240

	cout << "c3 hex code is " << c3.hex() << endl;
	cout << "crazy rbg is " << crazy.rgb() << endl;  // rgb(0,255,50)
	cout << "c4 value is " << c4.value() << endl; 

	int red = c3.changeRed(200);      // 13+200 = 213
	int green = c3.changeGreen(-60);  // 52-60 = -8, error correct to 0
	int blue = c3.changeBlue(300);    // 250+300 = 550, error correct to 255
	cout << "old colors " << red << " " << green << " " << blue << endl;
	cout << "new values " << c3.rgb() << endl;

	Color black = Color::black();   // class (static) method
	string blackcode = black.hex();
	cout << black.rgb() << "  " << blackcode << endl;

	c1 = Color(20, 20, 20);
	cout << "c1 is " << c1.value() << " " << c1.rgb() << " " << c1.hex() << endl;

	if (c1.value() == c2.value())
	  cout << "c1 == c2" << endl;
	if (black.value() == 0)
	  cout << "black is 0" << endl;

	c3 = c2;
	Color c5(c2);
	c3.changeRed(16);
	cout << "c2 is " << c2.rgb() << endl;
	cout << "c3 is " << c3.rgb() << endl;
	if (c3.rgb() == c2.rgb())
	  cout << "c3 same as c2" << endl;
	if (c5.hex() == c2.hex())
	  cout << "c5 same as c2" << endl;

	return 0;
}

/*  EXPECTED OUTPUT:

white is 16777215 rgb(255,255,255) #ffffff
c2 value is 123456
c2 rgb is rgb(1,226,64)
c2 hex is #01e240
c3 hex code is #0d34fa
crazy rbg is rgb(0,255,50)
c4 value is 3385958
old colors 13 52 250
new values rgb(213,0,255)
rgb(0,0,0)  #000000
c1 is 1315860 rgb(20,20,20) #141414
black is 0
c2 is rgb(1,226,64)
c3 is rgb(17,226,64)
c5 same as c2

*/
