#ifndef _PLAYER_H
#define _PLAYER_H

typedef struct
{
	char name[16];
	int playerID;
	int health;
	int mana;
	int experience;
	float x,y;
} Player;

#endif //_PLAYER_H
