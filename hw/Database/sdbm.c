/*
**
** sdbm: Simple Data Base Management
**
** Copyright (c) 2006-2007 by Peter H. Froehlich <phf@acm.org>.
** All rights reserved. The file COPYING has more details.
**
** Modified 6/5/2013 by James Doverspike
**
** If you get this interface as part of an assignment you
** can ignore the legalese above. :-)
*/

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sdbm.h"
#include "player.h"

/**
 * Minimum and maximum length of key and name strings,
 * including '\0' terminator.
 */
#define MIN_KEY_LENGTH 3
#define MAX_KEY_LENGTH 16


enum{
  NO_ERROR, FILE_NOT_FOUND, FILE_EXIST, FILE_NOT_CLOSED, FILE_NOT_OPENED, ALREADY_OPENED, KEY_NOT_EXIST, KEY_EXIST, KEY_NOT_FOUND, COULD_NOT_SYNC, NO_DATA, INVALID_PLAYERID
};
int ERROR_TYPE = NO_ERROR;
FILE *file=NULL;
int LAST=-1;
int CHECK_OPEN=0;
Player *dataPtr=NULL;
const Player NullPlayer={"\0",0,0,0,0,0,0};
int Pos=0, InsertPos=0;
int dataSize=0, syncStatus=0;
int startIns=0, endIns=0;



/*

/**
 * Create new database with given name. You still have
 * to sdbm_open() the database to access it. Return true
 * on success, false on failure.
 */
bool sdbm_create(const char *name)
{
  if(!CHECK_OPEN){
    if(!sdbm_open(name)){
      file=fopen(name, "w");
      sdbm_close();
      return 1;
    }
    sdbm_close();
    ERROR_TYPE = FILE_EXIST;
    return 0;
  }
  else{
    ERROR_TYPE=FILE_NOT_CLOSED;
    return 0;
  }
}
/**
 * Open existing database with given name. Return true on
 * success, false on failure.
 */
bool sdbm_open(const char *name)
{
  if(CHECK_OPEN){
    ERROR_TYPE=ALREADY_OPENED;
    return 0;
  }
  file=fopen (name, "r");
  if (!file){
    ERROR_TYPE=FILE_NOT_FOUND;
    return 0; 
  }
  fseek(file, 0, SEEK_END);
  dataSize = ftell(file);
  rewind(file);
  if (dataSize==0){
    dataPtr = (Player*)malloc(sizeof(Player)*10);
    dataSize=sizeof(Player)*10;
  }  
  else{
    //creates a back up file
    FILE *fptr=fopen("BACKUP","w");
    char *DATA = (char*)malloc(dataSize);
    fread(DATA,dataSize,1,file);
    fwrite(DATA,dataSize,1,fptr);
    free(DATA);
    fclose(fptr);
    //allocate array of pointers to dataPtr with the number of elements on the file*2.
    dataPtr = (Player*)malloc(dataSize*2);
    LAST=(dataSize/sizeof(Player))-1;
    //dataSize=sizeof(Player)*2;
  }
  
  //Allocating pointers to point to the data
  //dataPtr=(Player*)malloc(dataSize*sizeof(Player));
  rewind(file);
  CHECK_OPEN=fread(dataPtr, dataSize, 1, file);
  fclose(file);
  file=fopen (name, "w");
  CHECK_OPEN=1;
  return 1;
}

/**
 * Synchronize all changes in database (if any) to disk.
 * Useful if implementation caches intermediate results
 * in memory instead of writing them to disk directly.
 * Return true on success, false on failure.
 */
bool sdbm_sync()
{
  if (!CHECK_OPEN){
    ERROR_TYPE=FILE_NOT_OPENED;
    return 0;
  }


  // fwrite(datPtr,sizeof(Player)+sizeof(Player*),1,file);
  rewind(file);
  fwrite(dataPtr, (LAST+1)*sizeof(Player), 1, file);
  rewind(file);
  return 1;
}

/**
 * Close database, synchronizing changes (if any). Return
 * true on success, false on failure.
 */
bool sdbm_close()
{
  if(!sdbm_sync())
    {
      ERROR_TYPE=COULD_NOT_SYNC;
      return 0;
    }
  fclose(file);
  free(dataPtr);
  CHECK_OPEN=0;
  return 1;
  
}

/**
 * Return error code for last failed database operation.
 */
int sdbm_error()
{
  int temp=ERROR_TYPE;
  ERROR_TYPE=NO_ERROR;
  return temp;

}

/**
 * Is given key in database?
 */
bool sdbm_has(const char *key)
{
  if (!CHECK_OPEN){
    ERROR_TYPE=FILE_NOT_OPENED;
    return 0;
  }
  if (LAST==-1){
    ERROR_TYPE=NO_DATA;
    InsertPos=0;
    return 0;
  }
  int totalASCII=0;
  for(int j=0 ;key[j]!='\0';j++)
    totalASCII+=(int)(key[j]);  
  int first=0,last=LAST;
  int middle=(first+last)/2;
   while( first <= last )
   {
     if ( ((dataPtr+middle)->playerID)/1000000 < totalASCII )
         first = middle + 1;    
     else if (((dataPtr+middle)->playerID)/1000000  == totalASCII )
	break;
      else
         last = middle - 1;
 
      middle = (first + last)/2;
      if (middle == last || middle == first){
	endIns=last;
	startIns=first;
      }
   }
   if(first>last){
     ERROR_TYPE=KEY_NOT_FOUND;
     return 0;
   }
   for(int m=middle;( (dataPtr+ m)->playerID)/1000000 == totalASCII ;m++){
     if (strcmp(((dataPtr+m)->name), key)==0){
       Pos=m;
       return 1;
     }
     //  endIns=m;
   }
   for(int m=middle;( (dataPtr+m)->playerID)/1000000 == totalASCII ;m--){
     if (strcmp(((dataPtr+m)->name), key)==0){
       Pos=m;
       return 1;
     }
     //startIns=m;
   }
   ERROR_TYPE=KEY_NOT_FOUND;
   return 0;
}

/**
 * Get value associated with given key in database.
 * Return true on success, false on failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_get(const char *key, Player *value)
{
  
  if(!sdbm_has(key)){
    ERROR_TYPE=KEY_NOT_EXIST;
    return 0;
  }
  *value = *(dataPtr+Pos);
  int totalASCII=0;
  for(int j=0 ;key[j]!='\0';j++)
    totalASCII+=(int)(key[j]);
  value->playerID-= (totalASCII*1000000);
  return 1;
}
/**
 * Update value associated with given key in database
 * to given value. Return true on success, false on
 * failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_put(const char *key, const Player *value)
{
  if(!sdbm_has(key)){
    ERROR_TYPE=KEY_NOT_EXIST;
    return 0;
  }
  *(dataPtr+Pos) = *value;
  int totalASCII=0;
  for(int j=0 ;key[j]!='\0';j++)
    totalASCII+=key[j];
  (dataPtr+Pos)->playerID += (totalASCII*1000000);
  return 1;

}

/**
 * Insert given key and value into database as a new
 * association. Return true on success, false on
 * failure.
 *
 * Precondition: !sdbm_has(key)
 */
bool sdbm_insert(const char *key, const Player *value)
{
  if(sdbm_has(key)){
    ERROR_TYPE=KEY_EXIST;
    return 0;
  }
  if (LAST+1 ==((signed int)(dataSize/sizeof(Player)))){
    dataSize*=2;
    dataPtr=(Player*)realloc(dataPtr, dataSize);
  }
  
  int totalASCII=0;
  for(int j=0 ;key[j]!='\0';j++)
    totalASCII+=key[j];
  totalASCII *= 1000000;

  if (LAST!=-1)
    { 
      InsertPos=LAST+1;
      for (int i=0; i<=LAST; i++)	 
	if((value->playerID)+totalASCII<(dataPtr+i)->playerID)
	  {
	    InsertPos=i;
	    break;
	  }
    }
    
  /* if (LAST!=-1)
    {
      int first=startIns, last=endIns, middle;
      middle=(first+last)/2;
      while( first <= last )
	{
	  if (middle==first || middle==last)
	    break;
	  else if ( (value->playerID)+totalASCII > ((dataPtr+middle)->playerID) )
	    first = middle + 1;    
	  else if (((dataPtr+middle)->playerID)  == (value->playerID)+totalASCII )
	    {
	      ERROR_TYPE = INVALID_PLAYERID;
	      return 0;
	    }
	  else
	    last = middle - 1;
	  
	  middle = (first + last)/2;
	}
      if( ((dataPtr+middle)->playerID) < (value->playerID)+totalASCII)
	{
	  if(((dataPtr+last)->playerID) < (value->playerID)+totalASCII)
	    InsertPos=last+1;
	  else
	    InsertPos=last-1;
	}
      else
	InsertPos=first;
    }
  */
  LAST++;
  for(int t=LAST; InsertPos != t; t--)
    *(dataPtr+t)= *(dataPtr+t-1);
  //free(*(dataPtr+InsertPos));
  //dataPtr+InsertPos = (Player*)malloc(sizeof(Player));
  *(dataPtr+InsertPos) = *value;
  (dataPtr+InsertPos)->playerID += totalASCII;
  
  InsertPos=0;
  return 1;   
}

/**
 * Remove given key and associated value from database.
 * Return true on success, false on failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_remove(const char *key)
{
  if(!sdbm_has(key)){
    ERROR_TYPE=KEY_NOT_EXIST;
    return 0;
  }
  //here the code of searching will be.
  int temp = Pos;
  // if (temp!=LAST)  
  while(temp != LAST)
    {
      *(dataPtr+temp)= *(dataPtr+temp+1);
      temp++;
    }
  
  //else if (Pos==LAST)
  //*(*(dataPtr+LAST))=NullPlayer;
  *(dataPtr+LAST)=NullPlayer;
  LAST--;
  if (((unsigned int)LAST+1)==((dataSize/sizeof(Player)/4))){
    dataSize/=2; 
    dataPtr=(Player*)realloc(dataPtr, dataSize);
  }
  return 1;
  
}
