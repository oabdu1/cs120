#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sdbm.h"
#include "player.h"
/*Name:Osayd Abdu; Course: CS600.120; Date: 6/14/2013; Homework 4; 
(217)693-2577; Blackboard login: Oabdu1; Email: osaid3@live.com*/

int main(void)
{
  Player player1={"Osayd",727,100,99,1000,9.8f,2.9f};
  sdbm_create("HelloWorld.db");
  sdbm_create("HelloWorld.db");
  sdbm_open("File.db");
  sdbm_open("Hello.db");
  sdbm_has(player1.name);
  sdbm_open("File.db");
  sdbm_create("File.db");
  sdbm_open("File.db");
  Player player2={"James", 272, 100, 99, 1211, 8.2f, 3.8f};
  sdbm_has(player2.name);
  sdbm_remove(player2.name);
  sdbm_remove(player2.name);
  Player player={"",0,0,0,0,0,0};
  sdbm_close();
  sdbm_close();
  sdbm_open("File.db");
  Player player14={"Bm",112,112,112,113,113.4f,113.1f};
  sdbm_insert(player14.name, &player14);
  Player player16={"An",132,132,132,133,133.4f,133.1f};
  sdbm_insert(player16.name, &player16);
  Player player17={"Fi",142,142,142,143,143.4f,143.1f};
  sdbm_insert(player17.name, &player17);
  Player player18={"Na",152,152,152,153,153.4f,153.1f};
  sdbm_insert(player18.name, &player18);
  Player player19={"Cl",162,162,162,163,163.4f,163.1f};
  sdbm_insert(player19.name, &player19);
  Player player20={"Dk",172,172,172,173,173.4f,173.1f};
  sdbm_insert(player20.name, &player20);
  sdbm_has(player14.name);
  sdbm_remove(player20.name);
  sdbm_remove(player19.name);
  sdbm_remove(player18.name);
  sdbm_remove(player17.name);
  sdbm_remove(player16.name);
  sdbm_remove(player14.name);
  Player player5={"John",22,22,22,33,33.4f,33.1f};
  sdbm_insert(player5.name, &player5);
  sdbm_insert(player5.name, &player5);
  Player player6={"Lee",32,32,32,43,43.4f,43.1f};
  sdbm_insert(player6.name, &player6);
  Player player7={"Joanne",42,42,42,43,43.4f,34.1f};
  sdbm_insert(player7.name, &player7);
  Player player8={"James",52,2211,211,3,33.1f,31.1f};
  sdbm_insert(player8.name, &player8);
  Player player9={"Steve",62,62,62,63,63.4f,63.1f};
  sdbm_insert(player9.name, &player9);
  Player player10={"Peter",72,72,72,73,73.4f,73.1f};
  sdbm_insert(player10.name, &player10);
  Player player11={"Ali",82,82,82,83,83.4f,83.1f};
  sdbm_insert(player11.name, &player11);
  Player player12={"Faris",92,92,92,93,93.4f,93.1f};
  sdbm_insert(player12.name, &player12);
  Player player13={"Messi",102,102,102,103,103.4f,103.1f};
  sdbm_insert(player13.name, &player13); 
  Player player15={"Paul",182,182,182,183,183.4f,183.1f};
  sdbm_insert(player15.name, &player15);
  sdbm_insert(player1.name,&player1);
  sdbm_insert(player2.name, &player2);
  sdbm_get(player1.name,&player);
  sdbm_get(player2.name,&player);
  player1.mana=1;
  sdbm_put(player1.name,&player1);
  player2.mana=2;
  sdbm_put(player2.name,&player2);
  sdbm_sync();
  Player player50={"M",1032,1302,1023,3103,1303.43f,1033.1f};
  sdbm_insert(player50.name, &player50);
  sdbm_remove(player15.name);
  sdbm_remove(player13.name);
  sdbm_remove(player12.name);
  sdbm_remove(player11.name);
  sdbm_remove(player10.name);
  sdbm_remove(player9.name);
  sdbm_remove(player8.name);
  sdbm_remove(player7.name);
  sdbm_get(player7.name,&player);
  sdbm_put(player8.name,&player1);
  sdbm_close();
  sdbm_open("File.db");
  Player player31={"alexandra",1026,1602,1602,1603,1603.4f,6103.1f};
  sdbm_insert(player31.name, &player31); 
  Player player41={"zacchaeus",10424,1042,1402,1043,1034.4f,1403.1f};
  sdbm_insert(player41.name, &player41); 
  Player player42={"quennell",103,103,103,103,3.4f,13.1f};
  sdbm_insert(player42.name, &player42);
  sdbm_close(); 
  sdbm_open("TTTTTTTTTTTTTTT.db");
  printf("ERROR number: %d\n", sdbm_error());
  sdbm_close();
  sdbm_create("HelloWorld.db");
  printf("ERROR number: %d\n", sdbm_error());
  sdbm_close();
  return 0;
}



