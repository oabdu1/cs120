#ifndef _PLAYER_H
#define _PLAYER_H
/*Name:Osayd Abdu; Course: CS600.120; Date: 6/14/2013; Homework 4; (217)693-2577; Blackboard login: Oabdu1;
 Email: osaid3@live.com*/
typedef struct
{
	char name[16];
	int playerID;
	int health;
	int mana;
	int experience;
	float x,y;
} Player;

#endif //_PLAYER_H
